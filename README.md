# `viz`: the time tracking diary

`viz` is a small program for tracking, planning, and visualizing how time is spent, as well as
organizing ideas. It reads a markdown file formatted according to the format described by `viz
--help` and produces a color coded log over what days were spent on.

![screenshot](screenshot.png)
