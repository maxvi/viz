//! Parsing of `viz` files.

use crate::Activity;
use crate::Color;
use crate::Day;
use crate::Hour;
use crate::Log;
use crate::Parameter;

use std::error;
use std::fmt;
use std::collections::HashMap;

/// The heading for defining activities.
static ACTIVITY_HEADING: &'static str = "Activities";
/// The heading for defining parameters.
static PARAMETER_HEADING: &'static str = "Parameters";
/// The identifier that follows `ACTIVITY_IDENT` when the activity is consider productive.
static PRODUCTIVE_IDENT: &'static str = "productive ";
/// The bullet symbol that the parser reacts on.
static BULLET: &'static str = "+";

/// The parser state.
struct Parser<'a> {
    /// The part of the log that has been parsed.
    log: Log<'a>,
    /// The ending hour of the previous interval.
    ///
    /// When the parser reads an interval like `05-06`, this is set to `06`. It is used to detect
    /// syntax errors in the log.
    current_hour: u8,
    /// The last line that was parsed.
    previous_line: &'a str,
    /// The heading for the current section that is being parsed.
    heading: &'a str,
    /// Is this line a follow-up to a line specifying an hour?
    ///
    /// This sets to `true`, once the line specifying an hour is reached, and then switches to
    /// `false` when the next list item is encountered. Its purpose is to keep track of whether the
    /// line is bound to an hour item, in case of text wrapping.
    follow_up_to_hour: bool,
}

/// An error during parsing.
#[derive(Debug)]
pub enum ParseError {
    /// Syntax error at some line.
    Syntax(usize),
    /// Overwriting a parameter at some line.
    OverwritingParameter(usize),
    /// Unknown parameter at some line.
    UnknownParameter(usize),
    /// A faulty range at some line.
    BadRange(usize),
    /// Unknown activity at some line.
    UnknownActivity(usize),
    /// Faulty color at some line.
    BadColor(usize),
    /// Faulty number at some line.
    BadNumber(usize),
}

impl error::Error for ParseError {
    fn description(&self) -> &str {
        "parsing error"
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ParseError::Syntax(line) => write!(f, "parsing error at line {}", line),
            ParseError::OverwritingParameter(line) => write!(f, "overwriting parameter at line {}", line),
            ParseError::UnknownParameter(line) => write!(f, "unknown parameter at line {}", line),
            ParseError::BadRange(line) => write!(f, "bad hour range at line {}", line),
            ParseError::UnknownActivity(line) => write!(f, "unknown activity at line {}", line),
            ParseError::BadColor(line) => write!(f, "bad color at line {}", line),
            ParseError::BadNumber(line) => write!(f, "bad number at line {}", line),
        }
    }
}

impl Color {
    /// Parse a color from its name.
    ///
    /// This gives an error if the color is unknown.
    fn parse(s: &str) -> Result<Color, ()> {
        /// Parse a hexadecimal number.
        fn parse_hex(s: &str) -> Result<u8, ()> {
            let mut num = 0;
            for c in s.chars() {
                num *= 16;
                num += match c {
                    c @ '0' ..= '9' => c as u8 - '0' as u8,
                    c @ 'a' ..= 'f' => c as u8 - 'a' as u8 + 10,
                    c @ 'A' ..= 'F' => c as u8 - 'A' as u8 + 10,
                    _ => return Err(()),
                };
            }

            Ok(num)
        }

        // Consider a hex color.
        if s.starts_with('#') {
            // Check that the length is right.
            if s.len() != 7 {
                return Err(())
            }

            return Ok(Color::Rgb(parse_hex(&s[1..3])?, parse_hex(&s[3..5])?, parse_hex(&s[5..7])?));
        }

        match s {
            "black" => Ok(Color::Black),
            "white" => Ok(Color::White),
            "red" => Ok(Color::Red),
            "blue" => Ok(Color::Blue),
            "green" => Ok(Color::Green),
            "yellow" => Ok(Color::Yellow),
            "cyan" => Ok(Color::Cyan),
            "light red" => Ok(Color::LightRed),
            "light blue" => Ok(Color::LightBlue),
            "light green" => Ok(Color::LightGreen),
            "light yellow" => Ok(Color::LightYellow),
            "light cyan" => Ok(Color::LightCyan),
            "grey1" => Ok(Color::Grey1),
            "grey2" => Ok(Color::Grey2),
            "grey3" => Ok(Color::Grey3),
            _ => Err(()),
        }
    }
}

impl<'a> Log<'a> {
    /// Parse `data`.
    ///
    /// The parser works line-by-line using `Log::parse_line()`.
    pub fn parse(data: &'a str) -> Result<Log<'a>, ParseError> {
        let mut state = Parser {
            log: Log::default(),
            // We start with 24 to avoid the error check that ensures that the log of the previous
            // day was complete.
            current_hour: 24,
            previous_line: "",
            heading: "",
            follow_up_to_hour: false,
        };

        for (line_num, line) in data.lines().enumerate() {
            state.parse_line(line, line_num + 1)?;
        }

        Ok(state.log)
    }
}

impl<'a> Parser<'a> {
    /// Parse a line `line` with line number `line_num`.
    fn parse_line(&mut self, mut line: &'a str, line_num: usize) -> Result<(), ParseError> {
        // Trim the line for whitespaces in start and end.
        line = line.trim();

        // We write a simple parser in hand to maximize performance.

        // Keep track of the previous line.
        let previous_line = self.previous_line;
        self.previous_line = line;

        // We only react on lines starting with a particular markdown bullet point or section
        // heading.
        if line.starts_with(BULLET) {
            // Strip away the bullet.
            line = line[1..].trim_start();
        } else if line.starts_with("==") {
            // Update the current heading.
            self.heading = previous_line;
            // Add new day if indicated.
            if self.heading != ACTIVITY_HEADING && self.heading != PARAMETER_HEADING {
                // The line marks a new date, and is of the form `<date>`.

                // Ensure that the log of the previous day was completed.
                if self.current_hour != 24 {
                    return Err(ParseError::BadRange(line_num - 1));
                }
                self.current_hour = 0;

                // Add the day, starting empty.
                self.log.days.push(Day {
                    date: self.heading,
                    hours: Vec::new(),
                    productive_minutes: 0,
                    letters: Vec::new(),
                    parameters: HashMap::new(),
                });
            }

            return Ok(());
        } else if line.starts_with("- ") || line.starts_with("* ") {
            // We encountered a list item, and so we will no longer be a follow-up to an hour item.
            self.follow_up_to_hour = false;
            // Nothing on the line we care about.
            return Ok(());
        } else if self.follow_up_to_hour {
            // If the line is a follow-up to an hour item, scan line for specification of gained or
            // lost minutes, such as `-10`, as well as letter markings.
            let (gained_minutes, mut new_letters) =
                self.parse_follow_up_to_hour(line.split_whitespace(), line_num)?;
            let day = self.log.days.last_mut().unwrap();
            day.productive_minutes += gained_minutes;
            day.letters.append(&mut new_letters);
            // Nothing else on the line we care about.
            return Ok(());
        } else {
            // Nothing on the line we care about.
            return Ok(());
        }

        // The following handles the lines coming from bullet points.
        if self.heading == ACTIVITY_HEADING {
            // The line is a activity declaration of the form `<name>:<color>,<type color>`.

            // Detect if the activity is marked as productive.
            let productive = line.starts_with(PRODUCTIVE_IDENT);
            if productive {
                // Strip the identifier and trim the whitespaces inbetween. This makes `line` of
                // format `<name>:<color>,<type color>`, where we have stripped the start of
                // `<name>` to remove additional spacing after the identifier.
                line = line[PRODUCTIVE_IDENT.len()..].trim_start();
            }

            // Find the colon.
            let colon = line.find(':').ok_or(ParseError::Syntax(line_num))?;
            // Get the `<name>`, stripping potential whitespace before the colon.
            let activity = line[..colon].trim_end();
            // Give an error if the name contains a space.
            if activity.contains(' ') {
                return Err(ParseError::Syntax(line_num));
            }
            // Get the color data, stripping potential whitespace after the colon.
            let data = line
                .get(colon + 1..)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_start();

            // Find the comma.
            let comma = data.find(',').ok_or(ParseError::Syntax(line_num))?;
            // Get the `<color>`, stripping potential whitespace before the comma.
            let color = data[..comma].trim_end();
            // Get the `<type color>`, stripping potential whitespace after the comma.
            let type_color = data
                .get(comma + 1..)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_start();

            // Insert the activity into the table of activities.
            self.log.activities.insert(
                activity,
                Activity {
                    // Parse the colors.
                    color: Color::parse(color).map_err(|()| ParseError::BadColor(line_num))?,
                    type_color: Color::parse(type_color)
                        .map_err(|()| ParseError::BadColor(line_num))?,
                    productive,
                },
            );
        } else if self.heading == PARAMETER_HEADING {
            // The line is a parameter declaration of the form `<name>:<description>`.

            // Find the colon.
            let name;
            let description;
            if let Some(colon) = line.find(':') {
                // Get the `<name>`, stripping potential whitespace before the colon.
                name = line[..colon].trim_end();
                // Get the description, stripping potential whitespace after the colon.
                description = line
                    .get(colon + 1..)
                    .ok_or(ParseError::Syntax(line_num))?
                    .trim_start();
            } else {
                name = line;
                description = "";
            }

            // Insert the parameter into the list of parameters.
            self.log.parameters.insert(name, Parameter { description });
        } else if line.starts_with('[') {
            // The line marks an activity interval, and is of the form
            // `[{x}] <start>-<end>:<activity> [<text>] {+<added>} {-<subtracted>}`.

            // Make sure that we know that the subsequent lines are follow-ups to this hour item.
            // This is used for gained or lost minutes, in case of text-wrapping.
            self.follow_up_to_hour = true;

            // You cannot specify activities before you have declared a date.
            if self.log.days.is_empty() {
                return Err(ParseError::Syntax(line_num));
            }

            // Determine if the activity is checked yet.
            let planned = if line.starts_with("[x]") {
                false
            } else if line.starts_with("[ ]") {
                true
            } else {
                return Err(ParseError::Syntax(line_num));
            };
            // Trim potential whitespace.
            line = &line[3..].trim_start();

            // Find the dash.
            let dash_pos = line.find('-').ok_or(ParseError::Syntax(line_num))?;
            // Find the colon.
            let colon_pos = line.find(':').ok_or(ParseError::Syntax(line_num))?;
            // TODO: Give line numbers when `.parse()` gives error.
            // Find and parse `<start>`, stripping potential whitespace before the dash.
            let start: u8 = line
                .get(..dash_pos)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_end()
                .parse()
                .map_err(|_| ParseError::BadNumber(line_num))?;
            // Ensure that the range starts at where the last range ended.
            if start != self.current_hour {
                return Err(ParseError::BadRange(line_num));
            }

            // Find `<end>`, potentially stripping whitespace after the colon.
            let end_str = line
                .get(dash_pos + 1..colon_pos)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_start();
            // Parse `<end>` to a number.
            let end: u8 = end_str
                .parse()
                .map_err(|_| ParseError::BadNumber(line_num))?;

            // Ensure that the range is well-constructed.
            if start >= end || end > 24 {
                return Err(ParseError::BadRange(line_num));
            }
            // Update the previous end.
            self.current_hour = end;

            // An iterator over the words in `<activity> [+<added>] [-<subtracted>]`.
            let mut words = line
                .get(colon_pos + 1..)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_start()
                .split_whitespace();
            // Get the `<activity> [<text>] [+<added>] [-<subtracted>]` part, potentially trimming
            // whitespace in the start.
            let activity = if let Some(activity) = words.next() { activity } else {
                return Err(ParseError::Syntax(line_num));
            };
            // Trim potential whitespace.

            // Ensure that the activity actually exists.
            if !self.log.activities.contains_key(activity) {
                return Err(ParseError::UnknownActivity(line_num));
            }

            // Determine the productive minutes that have been gained.
            let (gained_minutes, mut new_letters) = self.parse_follow_up_to_hour(words, line_num)?;

            // Push the activities.
            let day = self.log.days.last_mut().unwrap();
            for _ in start..end {
                day.hours.push(Hour { activity, planned });
            }
            // Update productivity number.
            if self.log.activities[activity].productive {
                day.productive_minutes += (end - start) as isize * 60;
            }
            day.productive_minutes += gained_minutes;
            day.letters.append(&mut new_letters);
        } else {
            // The line marks a parameter declaration, and is of the form
            // `<parameter> = <value>`.

            // Find the `=`
            let eq_pos = line.find('=').ok_or(ParseError::Syntax(line_num))?;
            // Determine `<parameter>`
            let name = line
                .get(..eq_pos)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_end();
            // Determine `<value>`
            let value = line
                .get(eq_pos + 1..)
                .ok_or(ParseError::Syntax(line_num))?
                .trim_start();
            // Set the parameter in the log.
            let day = self.log.days.last_mut().unwrap();
            if !self.log.parameters.contains_key(name) {
                return Err(ParseError::UnknownParameter(line_num))
            }
            if let Some(_) = day.parameters.insert(name, value) {
                return Err(ParseError::OverwritingParameter(line_num));
            }
        }

        Ok(())
    }

    /// Extract gained/lost minutes and new letters from text following an hour declaration.
    ///
    /// This is a helper function for `parse_line`.
    fn parse_follow_up_to_hour<'b, I: Iterator<Item = &'b str>>(&mut self, words: I, line_num: usize)
        -> Result<(isize, Vec<char>), ParseError>
    {
        let mut gained_minutes = 0;
        let mut new_letters = Vec::new();

        for s in words {
            if s.starts_with(BULLET) {
                gained_minutes += s[1..].trim_start()
                    .parse::<isize>()
                    .map_err(|_| ParseError::BadNumber(line_num))?;
            } else if s.starts_with('-') {
                gained_minutes -= s[1..].trim_start()
                    .parse::<isize>()
                    .map_err(|_| ParseError::BadNumber(line_num))?;
            } else if s.starts_with('#') {
                // The letter to be added.
                let ltr = s[1..].chars()
                    .next()
                    .ok_or(ParseError::Syntax(line_num))?;
                // Introduce the letter into the log if it has not been used before.
                if !self.log.letters.contains(&ltr) {
                    self.log.letters.push(ltr);
                }
                new_letters.push(ltr);
            }
            // If we do not recognize the word, we will not return an error, since we want to
            // allow the user to specify objects to the activity such as `run 2.6 miles`, where
            // the `run` is the activity.
        }

        Ok((gained_minutes, new_letters))
    }
}
