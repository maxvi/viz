//! `viz` -- productivity time tracker

extern crate chrono;
extern crate clap;
#[macro_use]
extern crate failure;
extern crate termion;

mod format;
mod parser;

use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::{self, Read, Write};
use std::path::PathBuf;

use clap::{App, Arg};
use failure::Error;

/// The label of an activity.
type ActivityLabel<'a> = &'a str;

/// The help page for the `viz` file format.
static FORMAT_HELP: &'static str = r##"FORMAT:
    The format is largely compatible with markdown. Headings are declared by
        Heading
        =======

    Under the "Activities" heading, activities are declared. This is done by lines of the
    form:
        + [productive] <name>: <color>, <type color>
    <type color> is supposed to be shared between multiple activities, for example all activities
    considered "productive" could share the same <type color>. Colors are given using their name,
    e.g. "red", or by hexadecimal such as "#FF0000". [productive] is an optional identifier, which
    specifies that the activity should be a part of the hour count after each line.

    Other headings will start new entries representing days:
        <date>
        ======

    How hours are spent are specified by lines as follow. If the activity is completed, one uses
    the syntax
        + [x] <start hour>-<end hour>: <activity>
    If not,
        + [ ] <start hour>-<end hour>: <activity>
    In both cases, one may also specify gained or lost productive minutes by
        -<minutes>
    and/or
        +<minutes>
    in the end of the line.

    Furthermore, you can mark days by letters using # followed by the letter. This will then show
    up to the right of each day. For example, this can be useful for keeping track of whether you
    have done your daily exercise regime.

    We also allow <activity> to be followed by custom text. This can be used to further describe
    the activity.

    Under the "Parameters" heading, parameters may be declared by adding list items such as
        + PARAMETER NAME
    You may also add a description of the parameter by
        + PARAMETER NAME: DESCRIPTION
    Each day may then be assigned a parameter by writing
        + PARAMETER NAME = value
    during the day. The parameters can be accessed using the -P flag.

EXAMPLE FILE:
    Activities
    ==========
    + sleep: white, grey1
    + run: #B05111, grey1

    Thu, Oct 29
    ===========
    + [x] 00-04: sleep poorly -30
    + [x] 04-10: run 2.6 miles +30 +5 -10
      - run marathon
      - run two marathons
"##;

/// A color.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Color {
    /// Black (`black`).
    Black,
    /// White (`white`).
    White,
    /// Red (`red`).
    Red,
    /// Blue (`blue`).
    Blue,
    /// Green (`green`).
    Green,
    /// Yellow (`yellow`).
    Yellow,
    /// Cyan (`cyan`).
    Cyan,
    /// Light red (`light red`).
    LightRed,
    /// Light blue (`light blue`).
    LightBlue,
    /// Light green (`light green`).
    LightGreen,
    /// Light yellow (`light yellow`).
    LightYellow,
    /// Light cyan (`light cyan`).
    LightCyan,
    /// Lighter grey (`grey1`).
    Grey1,
    /// Medium grey (`grey2`).
    Grey2,
    /// Darker grey (`grey3`).
    Grey3,
    /// RGB (`#hex`).
    Rgb(u8, u8, u8),
}

impl Color {
    /// Give the greyness of a greyscale color.
    ///
    /// For example, `Rgb(x, x, x)` returns `x`. If it is not greyscale, it returns `!0`.
    fn greyness(&self) -> u8 {
        match *self {
            Color::Rgb(x, y, z) if x == y && y == z => x,
            // If one uses the old approach of `GreyN`, we are just returning values that allow
            // them to be sorted.
            Color::Grey1 => 1,
            Color::Grey2 => 2,
            Color::Grey3 => 3,
            _ => !0,
        }
    }
}

/// A user-declared parameter.
#[derive(Debug)]
pub struct Parameter<'a> {
    /// A description for parameter.
    description: &'a str,
}

/// An hour log.
///
/// This is the main structure of `viz`. It holds a vector of days, which in turn contain the
/// activities for each hour. Furthermore, it stores color data.
#[derive(Debug, Default)]
pub struct Log<'a> {
    /// Days, ordered as in the file.
    days: Vec<Day<'a>>,
    /// The metadata for activities.
    activities: HashMap<ActivityLabel<'a>, Activity>,
    /// Letters active in the letter matrix.
    letters: Vec<char>,
    /// Custom user-specified parameters for the day.
    parameters: HashMap<&'a str, Parameter<'a>>,
}

/// A day.
#[derive(Debug, Default)]
pub struct Day<'a> {
    /// The date of the day.
    ///
    /// There is no particular date format. This is left to the user.
    date: &'a str,
    /// Hours of the day.
    hours: Vec<Hour<'a>>,
    /// The number of minutes which were productive.
    productive_minutes: isize,
    /// Letters with which the day is marked (shown in the letter matrix).
    letters: Vec<char>,
    /// Custom user-specified parameters for the day.
    parameters: HashMap<&'a str, &'a str>,
}

/// How time is spent in an hour.
#[derive(Debug)]
struct Hour<'a> {
    /// The activity (say, `work`).
    activity: ActivityLabel<'a>,
    /// Is this simply planned?
    ///
    /// If `false`, the activity is supposed to be completed.
    planned: bool,
}

/// The metadata for a activity.
#[derive(Debug)]
struct Activity {
    /// The color that represents the label.
    color: Color,
    /// The color that represents the _type_ of the label.
    ///
    /// A "type" could for example be "productive" or "relax" and is shown right to the hour log.
    /// It is useful for recognizing the larger patterns in the log.
    type_color: Color,
    /// Is this activity considered productive?
    productive: bool,
}

/// Run `viz`.
fn main_err() -> Result<(), Error> {
    // Initialize stdout and stdin.
    let stdout = io::stdout();
    let mut stdout = stdout.lock();

    // Specify the CLI.
    let matches = App::new("viz")
        .version("0.1.0")
        .author("Max Vistrup")
        .about("Productivity time tracker")
        .arg(
            Arg::with_name("FILE")
                // TODO: Use a more automated way of specifying the default value.
                .help("the hour log [default: $VIZ_FILE]")
                .required(false)
                .index(1),
        )
        .arg(
            Arg::with_name("parameter")
                .short("P")
                .long("parameter")
                .value_name("PARAMETER")
                .help("print a parameter column"),
        )
        .arg(
            Arg::with_name("rofi")
                .short("r")
                .long("rofi")
                .help("print input tailored for rofi with markup"),
        )
        .arg(
            Arg::with_name("productive")
                .short("p")
                .long("productive")
                .help("dumps number of productive minutes for each day"),
        )
        .after_help(FORMAT_HELP)
        .get_matches();

    // Determine the path to the `viz` file.
    let path = if let Some(x) = matches.value_of("FILE") {
        PathBuf::from(x)
    } else {
        PathBuf::from(env::var("VIZ_FILE")?)
    };

    let parameter = matches.value_of("parameter");

    // Read the data of the file.
    let mut data = String::new();
    let mut file = File::open(path)?;
    file.read_to_string(&mut data)?;

    let log = Log::parse(&data)?;

    if let Some(parameter) = parameter {
        if !log.parameters.contains_key(parameter) {
            return Err(format_err!("unknown parameter '{}'", parameter));
        }
    }

    if matches.occurrences_of("rofi") == 0 {
        // Normal mode; print to terminal.
        log.print(format::Terminal(&mut stdout), parameter)?;
    } else {
        // Rofi mode.
        log.print(format::Rofi(&mut stdout), parameter)?;
    }

    if matches.occurrences_of("productive") != 0 {
        // Print the number of productive minutes for each day.
        for day in log.days {
            writeln!(&mut stdout, "{}", day.productive_minutes)?;
        }
    }

    Ok(())
}

fn main() -> Result<(), Error> {
    // Run `viz`.
    if let Err(err) = main_err() {
        // Handle error.
        eprintln!("Error: {}", err);
    }

    Ok(())
}
